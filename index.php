<?php

trait Hewan
{

  public $nama, $darah = 50, $jumlahKaki, $keahlian;

  function atraksi($elang)
  {
    return $this->nama . " pun kabur menghindari serangan " . $elang->nama . "<br><br>";
  }
}

trait Fight
{

  public $attackPower, $defencePower;

  function serang($harimau)
  {
    return "Seekor " . $this->nama . " menyerang " . $harimau->nama . "<br><br>";
  }
  function diserang($harimau)
  {
    $this->darah = $this->darah - ($harimau->attackPower / $this->defencePower);
    return "darah " . $harimau->nama . " tersisa " . $this->darah . "<br><br>";
  }
}

class Elang
{
  use Hewan, Fight;

  public function GetInfoHewan()
  {
    return "Seekor " . $this->nama . " memiliki jumlah kaki " . $this->jumlahKaki . " dengan keahlian " . $this->keahlian . " serta memiliki attack power = " . $this->attackPower . " dan defence power = " . $this->defencePower . "<br><br>";
  }
}

class Harimau
{
  use Hewan, Fight;

  public function GetInfoHewan()
  {
    return "Seekor " . $this->nama . " memiliki jumlah kaki " . $this->jumlahKaki . " dengan keahlian " . $this->keahlian . " serta memiliki attack power = " . $this->attackPower . " dan defence power = " . $this->defencePower . "<br><br>";
  }
}


$elang = new Elang();
$elang->nama = "Elang";
$elang->jumlahKaki = 2;
$elang->keahlian = "Terbang tinggi";
$elang->attackPower = 10;
$elang->defencePower = 5;

echo $elang->GetInfoHewan();

$harimau = new Harimau();
$harimau->nama = "Harimau";
$harimau->jumlahKaki = 2;
$harimau->keahlian = "Terbang tinggi";
$harimau->attackPower = 10;
$harimau->defencePower = 5;

echo $harimau->GetInfoHewan();

echo $elang->serang($harimau);
echo $harimau->atraksi($elang);
echo $elang->diserang($harimau);
